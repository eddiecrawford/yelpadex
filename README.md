### Eddie's Yelp Fusion API Assessment

A sample app using the Yelp Fusion GraphQL API to perform a simple search and return a list of businesses in an MVP pattern.
I used a `ViewComponent<ViewState, ViewEvents>` pattern as well to cleanly couple a custom view's events and view model.

Location is statically set to Denver. If I had more time I would have added an additional EditText field for location, or requesting permission and injecting GPS / etc to the search.

Tools used:

* Retrofit2 w/ Coroutines's Deferred Object instead of Retrofit's Call
* GraphQL w/o Apollo 
* Dagger2
* MVP-(VM)
* Glide for image loading / caching
* Mockito / Junit for testing

Wish I did my gradle build files in Kotlin DSL (`*.gradle.kts`) but didn't have the time. This is something that I haven't experimented with yet!

I've also never used GraphQL without the Apollo library, so this was a great learning experience to hook up corountines, GraphQL, and Retrofit into one solution with minimal boilerplate.
The testing suite is pretty much non-existent. I tried to give testing coroutines a shot, but have more to learn in that world.

Hope you enjoy!

Cheers,
Eddie

502.649.8813