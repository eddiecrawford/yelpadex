package com.eddie.yelpadex.model

data class SearchResponse(
    val total: Int = 0,
    val business: List<Business> = emptyList()
)