package com.eddie.yelpadex.model

data class Business(
    val id: String = "",
    val name: String = "",
    val photos: List<String> = emptyList(),
    val phone: String = "",
    val reviews: List<Review> = emptyList(),
    val rating: Float = 0f,
    val distance: Float = 0f
)