package com.eddie.yelpadex.model

/**
 * This is a simple wrapper class for the GraphQL data coming back. Since
 * the data returned is always in a JSON pair with the key equal to the name of
 * search itself.
 */

class SearchResponseWrapper(val search: SearchResponse = SearchResponse())