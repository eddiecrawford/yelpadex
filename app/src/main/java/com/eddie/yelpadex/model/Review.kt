package com.eddie.yelpadex.model

data class Review(
    val rating: Int = 0,
    val text: String = ""
)