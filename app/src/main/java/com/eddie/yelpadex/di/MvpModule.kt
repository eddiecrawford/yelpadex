package com.eddie.yelpadex.di

import com.eddie.yelpadex.mvp.main.MainActivity
import com.eddie.yelpadex.mvp.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MvpModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity
}