package com.eddie.yelpadex.di

import com.eddie.yelpadex.App
import com.eddie.yelpadex.api.di.NetworkComponent
import com.eddie.yelpadex.api.YelpService
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@AppScope
@Component(
    dependencies = [NetworkComponent::class],
    modules = [AndroidSupportInjectionModule::class, MvpModule::class]
)
interface AppComponent {
    fun inject(application: App)

    @Component.Builder
    interface Builder {
        fun networkingApiComponent(networkComponent: NetworkComponent): Builder

        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }
}