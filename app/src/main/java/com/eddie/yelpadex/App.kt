package com.eddie.yelpadex

import android.app.Activity
import android.app.Application
import com.eddie.yelpadex.api.AuthOkHttpInterceptor
import com.eddie.yelpadex.api.di.DaggerNetworkComponent
import com.eddie.yelpadex.di.AppComponent
import com.eddie.yelpadex.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    private lateinit var appComponent: AppComponent

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        initDagger()
        initLogger()

        instance = this
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .networkingApiComponent(
                DaggerNetworkComponent.builder()
                    .appContext(this)
                    .authInterceptor(AuthOkHttpInterceptor())
                    .build())
            .application(this)
            .build()
        this.appComponent.inject(this)
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree());
        } else {
            // We could build a custom logging tree here that could handle crash reporting / etc
            // Timber.plant(CrashReportingTree());
        }
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }
}