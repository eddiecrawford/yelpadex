package com.eddie.yelpadex.view

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.eddie.yelpadex.R
import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.util.Util
import com.eddie.yelpadex.view.base.ViewComponent
import kotlinx.android.synthetic.main.business_card_view.view.*

class BusinessCardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr), ViewComponent<Business, BusinessCardViewEvents> {

    private lateinit var viewEvents: BusinessCardViewEvents
    private lateinit var viewState: Business

    override fun updateViewState(viewState: Business) {
        this.viewState = viewState
        initName()
        initImage()
        initDistance()
        initRating()
        initReview()
    }

    override fun bindViewEvents(viewEvents: BusinessCardViewEvents) {
        this.viewEvents = viewEvents
        setOnClickListener { viewEvents.onClick(viewState) }
    }

    private fun initName() {
        tvName.text = viewState.name
    }

    private fun initDistance() {
        tvDistance.text = resources.getString(R.string.distance_miles, Util.metersToMiles(viewState.distance))
    }

    private fun initRating() {
        tvRating.text = String.format("%.1f", viewState.rating)
        rbStars.rating = viewState.rating
    }

    private fun initReview() {
        // Little formatting bug here, since the ending quotes get ellipsized on the
        // end of the review text. Open quotes with no closing quotes on the end.
        tvReview.text = String.format("\"%s\"", viewState.reviews.first().text)
    }

    private fun initImage() {
        Glide.with(this)
            .load(viewState.photos.first())
            .placeholder(R.drawable.ic_fast_food)
            .into(ivImage)
    }
}