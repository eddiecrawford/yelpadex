package com.eddie.yelpadex.view.base

/**
 * The underlying pattern to the VM in MVP-VM.
 */

interface ViewComponent<ViewState, ViewEvents> {
    fun updateViewState(viewState: ViewState)
    fun bindViewEvents(viewEvents: ViewEvents)
}