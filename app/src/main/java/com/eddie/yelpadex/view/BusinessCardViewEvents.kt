package com.eddie.yelpadex.view

import com.eddie.yelpadex.model.Business

interface BusinessCardViewEvents {
    fun onClick(business: Business)
}