package com.eddie.yelpadex.view

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import com.eddie.yelpadex.R
import com.eddie.yelpadex.view.base.ViewComponent
import kotlinx.android.synthetic.main.search_view.view.*

/**
 * A simple EditText holder that will pass back it's text and
 * initiate a search when the user hits enter/search on the keyboard.
 */

class SearchView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), ViewComponent<Unit, SearchViewEvents> {

    private val searchText get() = etSearchText.text.toString()

    private lateinit var viewEvents: SearchViewEvents

    init {
        LayoutInflater.from(context).inflate(R.layout.search_view, this, true)
    }

    override fun updateViewState(viewState: Unit) = Unit

    override fun bindViewEvents(viewEvents: SearchViewEvents) {
        this.viewEvents = viewEvents
        etSearchText.setOnEditorActionListener { _, actionId, keyEvent -> onEditorAction(actionId, keyEvent) }
    }

    private fun onEditorAction(actionId: Int, event: KeyEvent?): Boolean {
        if (((actionId == EditorInfo.IME_ACTION_DONE)
                    // Got to check for ActionDown to prevent multiple calls on a physical keyboard.
                    || ((actionId == EditorInfo.IME_NULL) && (event?.action == KeyEvent.ACTION_DOWN)))
            || (actionId == EditorInfo.IME_ACTION_SEARCH)) {
            viewEvents.onSearch(searchText)
            return true
        }
        return false
    }
}