package com.eddie.yelpadex.view

interface SearchViewEvents {
    fun onSearch(term: String)
}