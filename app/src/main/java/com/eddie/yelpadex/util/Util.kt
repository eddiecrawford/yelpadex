package com.eddie.yelpadex.util

class Util {

    companion object {
        fun metersToMiles(meters: Float): Double {
            return (meters / 1609.344)
        }
    }
}

// Could put some extension functions here if needed