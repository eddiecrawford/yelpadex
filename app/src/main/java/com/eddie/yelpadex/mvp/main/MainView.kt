package com.eddie.yelpadex.mvp.main

import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.mvp.base.MvpView

interface MainView : MvpView {
    fun onNetworkError()
    fun updateViewState(viewState: List<Business>)
    fun showSubmittedJobLoading()
    fun hideSubmittedJobLoading()
    fun hideKeyboard()
    fun showBusinessPhoneNumber(phoneNumberString: String)
}