package com.eddie.yelpadex.mvp.main

import com.eddie.yelpadex.api.YelpService
import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.view.BusinessCardViewEvents
import com.eddie.yelpadex.view.SearchViewEvents
import io.github.wax911.library.model.request.QueryContainerBuilder
import kotlinx.coroutines.*

const val SEARCH_LIMIT = 20

class MainPresenterImpl(private val yelpService: YelpService) : MainPresenter, BusinessCardViewEvents, SearchViewEvents {

    private lateinit var mainView: MainView

    private var currentSearchTermTotal = Int.MAX_VALUE
    private lateinit var searchTerm: String
    private var searchJob = Job()

    private var businessList = mutableListOf<Business>() // Our data

    override fun onAttach(mainView: MainView) {
        this.mainView = mainView
    }

    override fun fetch() {
        // We can get the total number of items possible from the search query. If we've hit that already,
        // let's not kick another pagination search call off, cause we know we've exhausted what can be returned.
        if (businessList.size >= currentSearchTermTotal) return

        mainView.showSubmittedJobLoading()
        val queryVars = buildSearchJobQueryParams(searchTerm)

        // Launch this on the IO scope, then post on the Main scope to the UI to update the view.
        searchJob = CoroutineScope(Dispatchers.IO).launch {
            val request = yelpService.businessesAsync(queryVars)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    when (response.data == null) {
                        true -> mainView.onNetworkError() // Generic error, probably marshalling
                        false -> {
                            currentSearchTermTotal = response.data!!.search.total
                            businessList.addAll(response.data!!.search.business)
                            // We want to make a copy of the list, or else Diff Util will be impossible
                            // (can't compare the same thing in memory (it will always be == ).
                            mainView.updateViewState(businessList.toList())
                        }
                    }
                } catch (e: Exception) {
                    mainView.onNetworkError()
                } finally {
                    mainView.hideSubmittedJobLoading()
                }
            }
        }
    }

    // A new search is coming in, let's clear our search list, reset the search term, and start over.
    // Oh, and don't forget to cancel the current Job if it's executing.
    override fun onSearch(term: String) {
        mainView.hideKeyboard()
        searchJob.cancel()
        businessList.clear()
        searchTerm = term
        fetch()
    }

    // If we reached the bottom let's kick off a new fetch sequence (unless we already did)
    override fun onScrolledToBottom() {
        if (!searchJob.isActive) fetch()
    }

    // Let's destory the reference to the Job, just in case it's currently running. Don't want any leaks.
    override fun onDetach() {
        searchJob.cancel()
    }

    // Implemented ViewEvent interface from the BusinessCardView class, bubbled up
    // from the BusinessCardView --> ViewHolder --> Adapter --> Presenter.
    override fun onClick(business: Business) {
        mainView.showBusinessPhoneNumber(business.phone)
    }

    private fun buildSearchJobQueryParams(term: String) =
        QueryContainerBuilder()
            .putVariable("term", term)
            .putVariable("location", "denver")
            .putVariable("limit", SEARCH_LIMIT)
            .putVariable("offset", businessList.size)
}