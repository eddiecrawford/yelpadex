package com.eddie.yelpadex.mvp.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.eddie.yelpadex.R
import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.view.BusinessCardView
import com.eddie.yelpadex.view.BusinessCardViewEvents
import com.eddie.yelpadex.view.base.ViewComponent

class BusinessAdapter : RecyclerView.Adapter<BusinessViewHolder>(), ViewComponent<List<Business>, BusinessCardViewEvents> {

    private var viewEvents: BusinessCardViewEvents? = null
    private var viewState: List<Business> = listOf()

    override fun updateViewState(viewState: List<Business>) {
        val diffCallback = DiffCallback(this.viewState, viewState)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.viewState = viewState

        diffResult.dispatchUpdatesTo(this)
    }

    override fun bindViewEvents(viewEvents: BusinessCardViewEvents) {
        this.viewEvents = viewEvents
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.business_card_view, parent, false) as BusinessCardView
        return BusinessViewHolder(view)
    }

    override fun onBindViewHolder(holder: BusinessViewHolder, position: Int) {
        viewEvents?.let { holder.bindViewEvents(it) }
        holder.updateViewState(viewState[position])
    }

    override fun getItemCount() = viewState.size
}

// The ViewComponent typing makes binding views super easy! We can easily send the ViewState to the custom view itself
// to bind itself. Much cleaner!! And can come from anywhere. IE - We don't have to duplicate binding logic in different
// places. It only has to live in the view class itself.
class BusinessViewHolder(private val businessCardView: BusinessCardView) :
        RecyclerView.ViewHolder(businessCardView), ViewComponent<Business, BusinessCardViewEvents> {
    override fun updateViewState(viewState: Business) = businessCardView.updateViewState(viewState)
    override fun bindViewEvents(viewEvents: BusinessCardViewEvents) = businessCardView.bindViewEvents(viewEvents)
}


// Generic DiffUtil! We will compare them based on the business's ID coming back from the API
class DiffCallback(
    private val oldRowList: List<Business>,
    private val newRowList: List<Business>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldRowList.size
    override fun getNewListSize() = newRowList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        (oldRowList[oldItemPosition].id == newRowList[newItemPosition].id)

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        (oldRowList[oldItemPosition] == newRowList[newItemPosition])
}