package com.eddie.yelpadex.mvp.main

import com.eddie.yelpadex.mvp.base.MvpPresenter

interface MainPresenter : MvpPresenter {
    fun onAttach(mainView: MainView)
    fun onDetach()
    fun fetch()
    fun onScrolledToBottom()
}