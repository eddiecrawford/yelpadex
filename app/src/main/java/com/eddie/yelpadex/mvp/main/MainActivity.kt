package com.eddie.yelpadex.mvp.main

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eddie.yelpadex.R
import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.view.BusinessCardViewEvents
import com.eddie.yelpadex.view.SearchViewEvents
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    @Inject lateinit var mainPresenter: MainPresenter

    private val businessAdapter = BusinessAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSearch()
        initRecyclerView()
        initPaginationListener()
        initErrorListener()

        mainPresenter.onAttach(this)
    }

    override fun onDestroy() {
        mainPresenter.onDetach()
        super.onDestroy()
    }

    private fun initSearch() {
        svSearchView.bindViewEvents(mainPresenter as SearchViewEvents)
    }

    // The presenter should handle click events from the view and delegate accordingly
    private fun initRecyclerView() {
        businessAdapter.bindViewEvents(mainPresenter as BusinessCardViewEvents)
        rvBusinesses.adapter = businessAdapter
        rvBusinesses.layoutManager = GridLayoutManager(this, 2)
    }

    // Would have loved to use androidx.paging, but went with a much simpler approach here.
    private fun initPaginationListener() {
        rvBusinesses.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    mainPresenter.onScrolledToBottom()
                }
            }
        })
    }

    // The try again part in the generic error handling.
    private fun initErrorListener() {
        bRetry.setOnClickListener {
            mainPresenter.fetch()
            flNetworkError.visibility = GONE
        }
    }

    // Just showcasing passing viewClicks up from the RecyclerView items, just shows the businesses phone #
    override fun showBusinessPhoneNumber(phoneNumberString: String) {
        Toast.makeText(this, phoneNumberString, Toast.LENGTH_SHORT).show()
    }

    // There was some sort of error fetching the data, throw a generic error and allow the user to try again
    override fun onNetworkError() {
        hideSubmittedJobLoading()
        flNetworkError.visibility = VISIBLE
    }

    override fun updateViewState(viewState: List<Business>) {
        flNetworkError.visibility = GONE
        businessAdapter.updateViewState(viewState)
    }

    override fun hideKeyboard() {
        if (currentFocus != null) {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                currentFocus!!.windowToken,
                0
            )
        }
    }

    // Spinnerz
    override fun showSubmittedJobLoading() {
        pbLoading.visibility = VISIBLE
    }

    override fun hideSubmittedJobLoading() {
        pbLoading.visibility = GONE
    }
}
