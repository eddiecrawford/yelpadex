package com.eddie.yelpadex.mvp.main

import androidx.appcompat.app.AppCompatActivity
import com.eddie.yelpadex.api.YelpService
import dagger.Module
import dagger.Provides

@Module
object MainModule {

    @Provides @JvmStatic
    fun provideActivity(mainActivity: MainActivity): AppCompatActivity = mainActivity

    @Provides @JvmStatic
    fun provideMainPresenter(yelpService: YelpService) : MainPresenter {
        return MainPresenterImpl(yelpService)
    }
}