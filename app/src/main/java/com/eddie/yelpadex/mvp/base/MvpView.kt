package com.eddie.yelpadex.mvp.base

/**
 * The V in MVP. Strictly responsible for presentation concerns, and hooking up user input events. User input events
 * should be delegated to the Presenter.
 */
interface MvpView