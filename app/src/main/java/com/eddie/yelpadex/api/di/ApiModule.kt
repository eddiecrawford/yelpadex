package com.eddie.yelpadex.api.di

import android.content.Context
import com.eddie.yelpadex.api.AuthOkHttpInterceptor
import com.eddie.yelpadex.api.YelpService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.github.wax911.library.converter.GraphConverter
import okhttp3.OkHttpClient
import retrofit2.Retrofit

const val yelpBaseUrl = "https://api.yelp.com/v3/"

@Module
object ApiModule {

    @Provides @NetworkScope @JvmStatic
    fun provideOkHttpClient(authInterceptor: AuthOkHttpInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(authInterceptor) // Handles auth
            .build()
    }

    @Provides @Reusable @JvmStatic
    fun provideRetrofit(okHttpClient: OkHttpClient, appContext: Context): Retrofit {
        return Retrofit.Builder()
            .baseUrl(yelpBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(GraphConverter.create(appContext)) // Antitrend's Retrofit / GraphQL converter
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // Jake Wharton's Call to Deferred adapter
            .build()

    }

    @Provides @Reusable @JvmStatic
    fun provideYelpService(retrofit: Retrofit): YelpService {
        return retrofit.create(YelpService::class.java)
    }
}