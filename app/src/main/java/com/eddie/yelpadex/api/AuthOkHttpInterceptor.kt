package com.eddie.yelpadex.api

import okhttp3.Interceptor
import okhttp3.Response

// This shouldn't go here. Would normally put it in my gradle.properties file, but leaving
// it here for now for easy of compiling the code base!
const val yelpKey = "ERUL2G_vbYsbMMTihu8cRUUxf6RIZhXo_jfcmV42G9oUxnHBXyb32Ehft-UNiWjhloXBgZCkGoquwsJ8VKrVek7Z7ada89Sll8Bo9lWAEGsQdtStOV58fNOd8uG_XHYx"

class AuthOkHttpInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val alteredRequest = chain
            .request()
            .newBuilder()
            .header(HEADER_AUTH_TOKEN, "Bearer $yelpKey")
            .build()

        return chain.proceed(alteredRequest)
    }

    companion object HeaderNames {
        const val HEADER_AUTH_TOKEN = "Authorization"
    }
}