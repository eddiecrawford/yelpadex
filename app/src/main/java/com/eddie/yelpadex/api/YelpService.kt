package com.eddie.yelpadex.api

import com.eddie.yelpadex.model.SearchResponseWrapper
import io.github.wax911.library.annotation.GraphQuery
import io.github.wax911.library.model.body.GraphContainer
import io.github.wax911.library.model.request.QueryContainerBuilder
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface YelpService {
    @POST("graphql")
    @GraphQuery("BusinessQuery")
    @Headers("Content-Type: application/json")
    fun businessesAsync(@Body request: QueryContainerBuilder): Deferred<GraphContainer<SearchResponseWrapper>>
}