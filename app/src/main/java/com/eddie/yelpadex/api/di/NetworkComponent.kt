package com.eddie.yelpadex.api.di

import android.content.Context
import com.eddie.yelpadex.api.AuthOkHttpInterceptor
import com.eddie.yelpadex.api.YelpService
import dagger.BindsInstance
import dagger.Component

@NetworkScope
@Component(modules = [ApiModule::class])
interface NetworkComponent {

    // Only thing we need from MainComponent in this scope
    fun getYelpService(): YelpService

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(appContext: Context): Builder

        @BindsInstance
        fun authInterceptor(authOkHttpInterceptor: AuthOkHttpInterceptor): Builder

        fun build(): NetworkComponent
    }
}