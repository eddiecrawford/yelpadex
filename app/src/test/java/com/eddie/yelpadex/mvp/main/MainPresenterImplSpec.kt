package com.eddie.yelpadex.mvp.main

import com.eddie.yelpadex.api.YelpService
import com.eddie.yelpadex.model.Business
import com.eddie.yelpadex.model.SearchResponse
import com.eddie.yelpadex.model.SearchResponseWrapper
import com.nhaarman.mockitokotlin2.*
import io.github.wax911.library.model.body.GraphContainer
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MainPresenterImplSpec {

    @Mock private lateinit var mockYelpService: YelpService
    @Mock private lateinit var mockView: MainView

    private val stubSearchTerm = "Foo Burger"

    private lateinit var subj: MainPresenterImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        subj = MainPresenterImpl(mockYelpService)
        subj.onAttach(mockView)
    }

    @Test
    fun `When search is initiated, Verify loading indicator is shown`() {
        subj.onSearch(stubSearchTerm)
        verify(mockView, times(1)).showSubmittedJobLoading()
    }

    
//    @Test
//    fun `When search is initiated, Verify view state is updated`() {
//        val deferred = CompletableDeferred<GraphContainer<SearchResponseWrapper>>()
//
//        doReturn(deferred).`when`(mockYelpService).businessesAsync(any())
//
//        runBlocking {
//            doReturn(buildResponse()).`when`(deferred).await()
//        }
//
//        subj.onSearch("burrito")
//
//        verify(mockView, times(1)).updateViewState(any())
//    }

    private fun buildResponse() : GraphContainer<SearchResponseWrapper> {
        val bizzes = buildViewState()

        return GraphContainer(
            data = SearchResponseWrapper(
                search = SearchResponse(
                    total = bizzes.size,
                    business = bizzes
                )
            ),
            errors = emptyList()
        )
    }

    private fun buildViewState() : List<Business> {
        val biz1 = Business(
            name = "Burrito Palace",
            distance = 412413f
        )

        val biz2 = Business(
            name = "Dairy King",
            distance = 1f
        )

        return listOf(biz1, biz2)
    }
}
